import Fish from './images/so_long_fish.jpg'

// Little bit extra for fun - image and additional response to show how it could be extended 
const SUCCESS_STR = [ 
  { success: 'thanks for all the fish', response: Fish },
  { success: 'life the universe and everything', response: '42' },
];

// Create a basic promise
const createPromise = ()=>{
  let resolve;
  let reject;
  const promise = new Promise((res, rej)=>{
    resolve = res;
    reject = rej;
  });

  return { resolve, reject,  promise }
}

// Fake Fetch
export const Fetch = (data)=>{
  const { resolve, reject,  promise } = createPromise();

  // Case insensitive search
  const success = SUCCESS_STR.find(({success})=>success === data.toLowerCase())

  if(success) {
    const { response } = success;
    resolve({response: { status: 200, text: ()=> response }});
  } else {
    reject({ response: { status: 500, error: 'Oops something has gone terribly wrong' }} )
  }

  return promise;
}
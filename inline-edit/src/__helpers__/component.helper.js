import React from "react";
import toJson from "enzyme-to-json";

// Runs basic snapshot test
export const SnapshotTest = snapshot => {
  describe("Basic component tests", () => {
    let component;
    beforeAll(() => {
      component = snapshot();
    });

    test("Should render", () => {
      expect(component.exists()).toBeTruthy();
    });

    test("snapshot matches", () => {
      // If you change component change props - see https://facebook.github.io/jest/docs/snapshot-testing.html
      // enzyme-to-json allows use of enzyme wrapped components

      expect(toJson(component)).toMatchSnapshot();
    });
  });
};

// Runs basic snapshot test
export const TestEvent = (elements, action) => {
  describe(`Tests event ${action} action`, () => {
    test("Should call correct handler", () => {
      const {component, handler} = elements();
      component.simulate(action);
      expect(handler).toHaveBeenCalled();
    });
  });
};




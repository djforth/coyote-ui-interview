import React from 'react';
import Icon from './index';
import { shallow } from 'enzyme';
import { SnapshotTest} from '../__helpers__/component.helper.js';

describe('Icon', () => {
  // Checks if no icon
  SnapshotTest(()=>shallow(<Icon type=""><p>foo</p></Icon>));

  // Checks if loading icon
  SnapshotTest(()=>shallow(<Icon type="loading"><p>foo</p></Icon>));

  // Checks if fail icon
  SnapshotTest(()=>shallow(<Icon type="fail"><p>foo</p></Icon>));

  // Checks if success icon
  SnapshotTest(()=>shallow(<Icon type="success"><p>foo</p></Icon>));
});
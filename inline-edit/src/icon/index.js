import React from 'react';
import PropTypes from 'prop-types';

// Sets SVG icons (probably better as SVG sprites or tweak build process to make them external, but felt in this instance was unnecessary)
const getIcon = (type)=>{
  switch(type){
    case 'fail': 
      return <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" className="hhgttg__icon hhgttg__icon--error"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg>;
    case 'success': 
      return <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" className="hhgttg__icon hhgttg__icon--success"><path d="m0 0h24v24h-24z" fill="none"/><path d="m9 16.2-4.2-4.2-1.4 1.4 5.6 5.6 12-12-1.4-1.4z"/></svg>;
    default:
      return <svg width="24" height="15" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" className="hhgttg__icon hhgttg__icon--loading" fill="white">
          <circle cx="15" cy="15" r="15">
              <animate attributeName="r" from="15" to="15"
                      begin="0s" dur="0.8s"
                      values="15;9;15" calcMode="linear"
                      repeatCount="indefinite" />
              <animate attributeName="fill-opacity" from="1" to="1"
                      begin="0s" dur="0.8s"
                      values="1;.5;1" calcMode="linear"
                      repeatCount="indefinite" />
          </circle>
          <circle cx="60" cy="15" r="9" fillOpacity="0.3">
              <animate attributeName="r" from="9" to="9"
                      begin="0s" dur="0.8s"
                      values="9;15;9" calcMode="linear"
                      repeatCount="indefinite" />
              <animate attributeName="fill-opacity" from="0.5" to="0.5"
                      begin="0s" dur="0.8s"
                      values=".5;1;.5" calcMode="linear"
                      repeatCount="indefinite" />
          </circle>
          <circle cx="105" cy="15" r="15">
              <animate attributeName="r" from="15" to="15"
                      begin="0s" dur="0.8s"
                      values="15;9;15" calcMode="linear"
                      repeatCount="indefinite" />
              <animate attributeName="fill-opacity" from="1" to="1"
                      begin="0s" dur="0.8s"
                      values="1;.5;1" calcMode="linear"
                      repeatCount="indefinite" />
          </circle>
      </svg>;
  }
}

const Icon = ({ children, type })=> <div className="hhgttg__icon-wrapper">
    {children}
    {type !== '' && getIcon(type)}
  </div>;

Icon.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]).isRequired,
  type: PropTypes.string.isRequired,
};

export default Icon;
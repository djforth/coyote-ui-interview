import React from 'react';

import Button from './index';
import { shallow } from 'enzyme';
import { SnapshotTest, TestEvent } from '../__helpers__/component.helper.js';

describe('Button', () => {
  let component, handler;

  beforeAll(() => {
    handler = jest.fn();
    component = shallow(<Button handler={handler} text="foo" />);
  });

  SnapshotTest(()=>component);
  TestEvent(()=>({
    component: component,
    handler
  }), 'click');
});
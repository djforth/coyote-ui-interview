import React from 'react';
import PropTypes from 'prop-types';

// Using a button as it's better for accessibility than binding to a p tag 
const Button = ({handler, text})=><button className='hhgttg__text' title="click to edit" onClick={handler}>{text}</button>

Button.propTypes = {
  handler: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired
};

export default Button;
import React from 'react';
import Success from './index';
import { shallow } from 'enzyme';
import { SnapshotTest} from '../__helpers__/component.helper.js';

describe('Icon', () => {
  // Checks if no icon
  SnapshotTest(()=>shallow(<Success value="some text" />));

  // Checks if loading icon
  SnapshotTest(()=>shallow(<Success value="image.jpg" />));

})
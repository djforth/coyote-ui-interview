import React from 'react';
import PropTypes from 'prop-types';

const Success = ({value})=>{
  if(/.jpg$/.test(value)) return <img src={value} alt="So long and thanks for all the fish" className="hhgttg__success-img" />
  
  return <h2 className="hhgttg__success">{value}</h2> 
}

Success.propTypes = {
  value: PropTypes.string.isRequired
};

export default Success;
import React from 'react';
import PropTypes from 'prop-types';

const Input = ({blurHandler, handler, keyHandler, value})=><input 
    className='hhgttg__input'
    value={value}
    onBlur={blurHandler} 
    onChange={handler} 
    onKeyDown={keyHandler} 
    type="text" 
  />;

Input.propTypes = {
  blurHandler: PropTypes.func.isRequired,
  handler: PropTypes.func.isRequired,
  keyHandler: PropTypes.func.isRequired,
  value: PropTypes.string
};

export default Input;
import React from 'react';
import Input from './index';
import { shallow } from 'enzyme';
import { SnapshotTest, TestEvent } from '../__helpers__/component.helper.js';

describe('Input', () => {
  let component, handler, blurHandler, keyHandler;

  beforeAll(() => {
    blurHandler = jest.fn();
    handler = jest.fn();
    keyHandler = jest.fn();
    component = shallow(<Input handler={handler} blurHandler={blurHandler} keyHandler={keyHandler} />);
  });

  afterEach(()=>{
    handler.mockClear();
    blurHandler.mockClear();
    keyHandler.mockClear();
  });

  // Checks snapshot
  SnapshotTest(()=>component);

  // Test change event
  TestEvent(()=>({
    component: component,
    handler
  }), 'change');

  // Test keyDown event
  TestEvent(()=>({
    component: component,
    handler: keyHandler
  }), 'keyDown');

  // Test blur event
  TestEvent(()=>({
    component: component,
    handler: blurHandler
  }), 'blur');
});
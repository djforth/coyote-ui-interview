import React from 'react';
import {act} from '@testing-library/react'
import { shallow, mount } from 'enzyme';
import { SnapshotTest } from './__helpers__/component.helper';
import App from './App';

jest.useFakeTimers()

describe('Test App', () => {
  let component;
  describe('basic test', () => {
    beforeAll(() => {
      component = shallow(<App />);
    });

    SnapshotTest(()=>component);
  });
  
  describe('Behaviour tests', () => {
    beforeAll(()=>{
      component = mount(<App />);
    });

    test('clicking on button should show input', () => {
      let button  = component.find('button');
      button.simulate('click');

      const input  = component.find('input');
      button  = component.find('button'); // 
      expect(component.exists('button')).toEqual(false); // button hidden
      expect(input.is('input')).toEqual(true); // input visible
    });

    test("adding 'thanks for all the fish' return success", async () => {
      const input  = component.find('input');
      input.simulate('change', {target:{ value: 'thanks for all the fish'}}); // inserts text
      await act( async () => {
        input.simulate('keyDown', {key: 'Enter'});
        jest.runAllTicks();
        jest.runAllTimers();
      });

      component.update();
      expect(component.exists('button')).toEqual(true); // button visible
      expect(component.exists('input')).toEqual(false); // input hidden
      
      const img = component.find(".hhgttg__success-img");
      expect(img.is('img')).toEqual(true);
      expect(img.html()).toEqual('<img src="so_long_fish.jpg" alt="So long and thanks for all the fish" class="hhgttg__success-img">');

      const icon = component.find(".hhgttg__icon-wrapper");
      expect(icon.exists('svg')).toEqual(true);
      const svg = component.find("svg");
      expect(svg.hasClass('hhgttg__icon--success')).toEqual(true);
    });

    test("adding 'life the universe and everything' return success", async () => {
      let button  = component.find('button');
      button.simulate('click');

      const input  = component.find('input');
      input.simulate('change', {target:{ value: 'life the universe and everything'}}); // inserts text
      await act( async () => {
        input.simulate('keyDown', {key: 'Enter'});
        jest.runAllTicks();
        jest.runAllTimers();
      });

      component.update();

      expect(component.exists('button')).toEqual(true); // button visible
      expect(component.exists('input')).toEqual(false); // input hidden
      
      const h2 = component.find(".hhgttg__success");
      expect(h2.is('h2')).toEqual(true);
      expect(h2.text()).toEqual('42');

      const icon = component.find(".hhgttg__icon-wrapper");
      expect(icon.exists('svg')).toEqual(true);
      const svg = component.find("svg");
      expect(svg.hasClass('hhgttg__icon--success')).toEqual(true);
    });
  });

  test('adding random text returns error', async () => {
      let button  = component.find('button');
      button.simulate('click');
      const input  = component.find('input');
      input.simulate('change', {target:{ value: 'foo'}}); // inserts text
      await act( async () => {
        input.simulate('blur');
        jest.runAllTicks();
        jest.runAllTimers();
      });

      component.update();

      expect(component.exists('button')).toEqual(true); // button visible
      expect(component.exists('input')).toEqual(false); // input hidden
      
      const error = component.find(".hhgttg__error");
      expect(error.is('p')).toEqual(true);
      expect(error.text()).toEqual('Oops something has gone terribly wrong');

      const icon = component.find(".hhgttg__icon-wrapper");
      expect(icon.exists('svg')).toEqual(true);
      const svg = component.find("svg");
      expect(svg.hasClass('hhgttg__icon--error')).toEqual(true);
    });

});
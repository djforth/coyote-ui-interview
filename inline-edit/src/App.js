import React, { useState } from 'react';
import dontPanic from './images/dont_panic.jpg';
import './App.css';

import {Fetch} from './logic';

// Components
import Button from './button';
import Icon from './icon';
import Input from './input';
import Success from './success';

// Basic curry function
const curry = (fn, ...args) => (fn.length <= args.length ? fn(...args) : (...more) => curry(fn, ...args, ...more));

// Handle error from fetch
const handleError = (setError, setSuccess, setIcon, setInputText, {response})=>{
  setIcon('fail'); 
  setSuccess('');
  setInputText('Hello world');
  setError(response.error);
}

// Handle 
const handleSuccess = (setSuccess, setIcon, setError, {response})=>{
  setIcon('success');
  setError('')
  setSuccess(response.text());
}

function App() {
  // Hooks
  const [edit, setEdit] = useState(false);
  const [error, setError] = useState('');
  const [icon, setIcon] = useState('');
  const [inputText, setInputText] = useState('Hello World');
  const [success, setSuccess] = useState('');
  // set handlers
  const errorHandler = curry(handleError, setError, setSuccess, setIcon, setInputText);
  const successHandler = curry(handleSuccess, setSuccess, setIcon, setError);

  const fetchHandler = async ()=>{
    setIcon('loading');
    // 2 sec pause to show loading
    setTimeout(async ()=>{
      const request = await Fetch(inputText).catch(errorHandler);
   
      if(request) {
        successHandler(request);
      } 
    }, 2000)
    
  }

  const editHandler = ()=>{
    setEdit(true)
  }

  const inputHandler = (e)=>{
    const value = e.target.value;
    setInputText(value);
  };

  const keyHandler = async ({key})=>{
    if(key !== 'Enter') return;
    setEdit(false);
    await fetchHandler();
  }

  const blurHandler = async ()=>{
    setEdit(false);
    await fetchHandler();
  }


  return (
    <div className="hhgttg">
      <header className="hhgttg__header">
        <img src={dontPanic} className="hhgttg__logo" alt="Don't Panic" />
      </header>
      <Icon type={icon} >
        {!edit && <Button handler={editHandler} text={inputText} /> }
        {edit && 
          <Input 
            blurHandler={blurHandler}
            handler={inputHandler}
            keyHandler={keyHandler}
            value={inputText} 
          />
        }
       </Icon>
      {error && <p className={"hhgttg__error"}>{error}</p>}
      {success && <Success value={success}/>}
    </div>
  );
}

export default App;

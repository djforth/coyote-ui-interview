import {Fetch} from './logic';
import Fish from './images/so_long_fish.jpg';

describe('logic', () => {
  test('should fail if data not correct', async () => {
    await Fetch('foo').catch(({response})=>{
      const {status, error} = response;
      expect(error).toEqual('Oops something has gone terribly wrong');
      expect(status).toEqual(500);
    });
  });

  test('should fail if data not correct', async () => {
    const { response } = await Fetch('Thanks for all the fish');
    expect(response.text()).toEqual(Fish);
    expect(response.status).toEqual(200);
  });

  test('should fail if data not correct', async () => {
    const { response } = await Fetch('Life the universe and everything');
    expect(response.text()).toEqual('42');
    expect(response.status).toEqual(200);
  });
  
});